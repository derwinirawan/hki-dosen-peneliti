# Hak Kekayaan Intelektual (HKI) untuk dosen dan peneliti

**Oleh: Dasapta Erwin Irawan ([ORCID](http://djpen.kemendag.go.id/app_frontend/contents/99-hak-kekayaan-intelektual))** 

**Institut Teknologi Bandung / RINarxiv** 

Keterangan foto banner: jejaring sungai dari udara (arsip NASA, public domain)

Dokumen ini disebarkan dengan lisensi (izin): [CC-0 Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)

**Disclaimer**

*Saya bukan pakar atau konsultan HKI. Yang saya sampaikan di sini adalah prinsip baku HKI, yang saya dapatkan dari pelatihan singkat, pencarian daring, dan beberapa contoh praktek tatakelola yang baik. Adapun variasi bisa saja terjadi, tetapi tidak menyalahi prinsip baku yang tercantum dalam laman [Peraturan-Peraturan terkait Hak Cipta Ditjen Kekayaan Intelektual Kemenhukham RI](https://dgip.go.id/peraturan-perundang-undangan-terkait-hak-cipta). Mohon ibu dan bapak kritis tentang apa yang saya sampaikan dan menyampaikan segera kepada saya bila menemukan kekurangan atau kesalahan. Terima kasih.* 

# Bagian 1 untuk kuliah daring dan material daring lainnya yang dibuat untuk tujuan pendidikan dan penelitian

## **Pendahuluan**

Berkaitan dengan penyusunan materi perkuliahan daring yang saat ini menjadi salah satu pilihan dosen/peneliti dalam proses belajar mengajar. Salah satu hal yang sering menjadi pertanyaan adalah Hak Kekayaan Intelektual (HKI). Materi tentang HKI ini merupakan gabungan antara beberapa materi yang sebelumnya telah tayang di [blog saya](http://dasaptaerwin.net/wp/2020/07/beberapa-video-tentang-hki.html), serta materi lainnya yang dibuat untuk acara Seminaring Edunex ITB dan laman LPIK ITB. Walaupun pada awalnya dibuat untuk sivitas akademik ITB, materi ini sifatnya sebenarnya bersifat generik. Saya akan menyampaikan HKI dari sudut pandang pengguna dan pencipta.

## **Bagian 0 Dasar-dasar HKI**

### **Apakah Hak Kekayaan Intelektual (HKI) atau Hak Atas Kekayaan Intelektual (HAKI) itu?**

- Sebenarnya istilah HKI/HAKI tidak dikenal dalam [UU 28/2014 tentang Hak Cipta](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/uu_hc_%2028_2014.pdf). Istilah yang dipakai adalah **Hak Cipta** atau *copyright*.
- Arti istilah HKI yang ada dalam laman resmi pemerintah Indonesia, salah satunya tertulis di [laman Kementerian Perdagangan](http://djpen.kemendag.go.id/app_frontend/contents/99-hak-kekayaan-intelektual), selain laman-laman lain termasuk laman perguruan tinggi (misal: [IPB](https://dik.ipb.ac.id/ki-hki/), [ITB](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwi-rcvfpdXqAhVrlEsFHWDMBo4QFjAAegQIBRAB&url=https%3A%2F%2Farsip.itb.ac.id%2Fuploads%2Farsip%2F41415IF3280_9_Haki.pdf&usg=AOvVaw2NmD63-duM4oorTJ25cgE9)).
- HKI menurut [Kementerian Perdagangan](http://djpen.kemendag.go.id/app_frontend/contents/99-hak-kekayaan-intelektual): "Hak kekayaan intelektual (HKI) terbagi menjadi dua kategori, yaitu hak cipta dan hak kekayaan industri. Hak cipta adalah hak ekslusif bagi pencipta atau penerima hak untuk mengumumkan atau memperbanyak ciptaannya atau memberikan izin untuk itu dengan tidak mengurangi pembatasan-pembatasan menurut peraturan perundang-undangan yang berlaku. Sedangkan hak kekayaan industri terdiri dari hak: Paten, Merek, Desain industri, Desain tata letak sirkuit terpadu, Rahasia dagang, Varietas tanaman."
- HKI menurut [IPB](https://dik.ipb.ac.id/ki-hki/): "Secara sederhana kekayaan intelektual merupakan kekayaan yang timbul atau lahir dari kemampuan intelektual manusia. Karya-karya yang timbul atau lahir dari kemampuan intelektual manusia dapat berupa karya-karya di bidang teknologi, ilmu pengetahuan, seni dan sastra. Karya-karya tersebut dilahirkan atau dihasilkan atas kemampuan intelektual manusia melalui curahan waktu, tenaga, pikiran, daya cipta, rasa dan karsanya."
- Karena itu, mari kita cari definisi HKI atau [Intellectual Property Rights dari WTO](https://www.wto.org/english/tratop_e/trips_e/intel1_e.htm), yaitu: "Intellectual property rights are the rights given to persons over the creations of their minds. They usually give the creator an exclusive right over the use of his/her creation for a certain period of time." Dalam laman yang sama disebut pula bahwa **IPR terdiri dari *copyright* and *industrial property***.

**Kesimpulan:** 

- HKI bersifat luas, terdiri dari Hak Cipta dan Hak Kekayaan Industri. Jadi
- Hak Cipta adalah bagian dari HKI.
- Mayoritas HKI dari dosen dan peneliti adalah Hak Cipta.

### Terdiri dari apa sajakah?

HKI terdiri dari Hak Cipta dan Hak Kekayaan Industri: Paten, Merek, Desain industri, Desain tata letak sirkuit terpadu, Rahasia dagang, Varietas tanaman.

### Apa landasan hukum HKI di dunia dan di Indonesia?

- Berbagai landasan hukum HKI di Indonesia yang menurut saya sangat terkait dengan kegiatan dosen dan peneliti sehari-hari dapat dilihat pada [laman ini](https://dgip.go.id/peraturan-perundang-undangan-terkait-hak-cipta), yang terdiri dari:
    1. [UU Nomor 28 Tahun 2014 tentang Hak Cipta](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/uu_hc_%2028_2014.pdf)
    2. [Peraturan Pemerintah Republik Indonesia Nomor 16 Tahun 2020 tentang Pencatatan Ciptaan dan Produk Hak Terkait](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/pp_7_1989_ttg_perubahan_atas_pp_%20no_%2014_%20th_%201986.pdf)
    3. [Peraturan Pemerintah Republik Indonesia Nomor 7 Tahun 1989 tentang Perubahan atas Peraturan Pemerintah Nomor 14 Tahun 1986 tentang Dewan Hak Cipta ditetapkan Tanggal 5 April 1989](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/pp_7_1989_ttg_perubahan_atas_pp_%20no_%2014_%20th_%201986.pdf)
    4. [Peraturan Pemerintah Republik Indonesia Nomor 1 Tahun 1989 tentang Penterjemahan dan/atau Perbanyakan Ciptaan untuk Kepentingan Pendidikan, Ilmu Pengetahuan, Penelitian dan Pengembangan ditetapkan Tanggal 14 Januari 1989](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/pp_no.1_thn_1989.pdf)
    5. Peraturan Pemerintah Republik Indonesia Nomor 14 Tahun 1986 tentang Dewan Hak Cipta ditetapkan Tanggal 6 Maret 1986
    6. Peraturan Pemerintah Republik Indonesia Nomor 28 Tahun 2019 tentang Jenis Dan Tarif Atas Penerimaan Negara Bukan Pajak Yang Berlaku Pada Kementerian Hukum dan Hak Asasi Manusia
    7. [Peraturan Bersama Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Dan Menteri Komunikasi dan Informasi Republik Indonesia Nomor 14 Tahun 2015 Nomor 26 Tahun 2015 Tentang Pelaksanaan Penutupan Konten Dan/Atau Hak Akses Pengguna Pelanggaran Hak Cipta Dan/Atau Hak Terkait Dalam Sistem Elektronik](https://dgip.go.id/images/ki-images/pdf-files/hak_cipta/uu_pp/bn1040-2015.pdf)
    8. Peraturan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia No. 36 Tahun 2018 Tentang Tata Cara Permohonan dan Penerbitan Izin Operasional Serta Evaluasi Lembaga Manajemen Kolektif
    9. Keputusan Presiden Republik Indonesia Nomor 74 Tahun 2004 tentang Pengesahan WIPO Performances and Phonograms Treaty, 1996/Traktat WIPO Mengenai Pertunjukan dan Perekam Suara
    10. Traktat WIPO Mengenai Pertunjukan dan Perekaman Suara

### Jadi apa saja yang penting tentang HKI?

- Mulai titik ini, mohon HKI yang dibahas adalah Hak Cipta (HC) atau *copyright*. Menurut UU 28/2014 tentang Hak Cipta Bab I Ketentuan Umum (lihat tautan di atas)
    - Hak Cipta adalah hak eksklusif pencipta yang timbul secara otomatis berdasarkan prinsip deklaratif setelah suatu ciptaan diwujudkan dalam bentuk nyata tanpa mengurangi pembatasan sesuai dengan ketentuan peraturan perundang-undangan.
    - Pencipta adalah seorang atau beberapa orang yang secara sendiri- sendiri atau bersama-sama menghasilkan suatu ciptaan yang bersifat khas dan pribadi.
    - Ciptaan adalah setiap hasil karya cipta di bidang ilmu pengetahuan, seni, dan sastra yang dihasilkan atas inspirasi, kemampuan, pikiran, imajinasi, kecekatan, keterampilan, atau keahlian yang diekspresikan dalam bentuk nyata.
    - Pemegang Hak Cipta adalah Pencipta sebagai pemilik Hak Cipta, pihak yang menerima hak tersebut secara sah dari Pencipta, atau pihak lain yang menerima lebih lanjut hak dari pihak yang menerima hak tersebut secara sah.
    - Pengumuman adalah pembacaan, penyiaran, pameran, suatu ciptaan dengan menggunakan alat apapun baik elektronik atau non elektronik atau melakukan dengan cara apapun sehingga suatu ciptaan dapat dibaca, didengar, atau dilihat orang lain.
    - Lisensi adalah izin tertulis yang diberikan oleh Pemegang Hak Cipta atau Pemilik Hak Terkait kepada pihak lain untuk melaksanakan hak ekonomi atas Ciptaannya atau produk Hak Terkait dengan syarat tertentu.
    - Royalti adalah imbalan atas pemanfaatan Hak Ekonomi suatu Ciptaan atau Produk Hak Terkait yang diterima oleh pencipta atau pemilik hak terkait.

**Sampai sini apa yang bisa disimpulkan?**

- Bahwa semua karya yang dihasilkan seorang dosen/peneliti memiliki Hak Cipta
- Siapa pemilik Hak Ciptanya? Adalah penulis atau tim penulis
- Apa saja sifat dasar Hak Cipta?
    - harus ada wujudnya, bisa berbentuk: karya tulis, patung, lukisan, lagu, dll.
    - bersifat otomatis, ketika penulis membubuhkan namanya atas suatu karya atau ciptaan.
    - bersifat eksklusif, yaitu pemilik dari suatu karya/ciptaan adalah hanya penulis/tim penulis. Secara tidak langsung ini berarti, siapapun yang ingin menggunakan suatu karya/ciptaan ibu dan bapak, harus minta izin kepada ibu dan bapak.
    - Harus diumumkan. Di sini bentuk pengumuman bisa dilakukan dengan berbagai cara, misal: menerbitkan makalah di jurnal atau seminar, menyalin tautan makalah di media sosial (medsos), memasang foto di medsos, dll kegiatan yang sejenis. Di sini bentuk pengumuman yang dikaitkan dengan publikasi sudah sangat berbeda dengan era sebelumnya. Publikasi sudah dapat dilakukan dengan mudah dan instan. Makna publikasi bukan lagi harus melalui suatu mesin yang bernama mesin cetak.
    - Kenapa harus diumumkan? Satu-satunya tujuan adalah agar orang lain mengetahuinya.

### Tentang Pencipta

Entah kenapa pasal ini tidak diletakkan di awal, tetapi di Bab IV atau pasal 31 sampai pasal 37. Ini pasal yang menarik, karena ternyata ada beberapa variasi. Antara Pencipta dan Pemegang HC bisa orang yang berbeda.

- Kecuali terbukti sebaliknya, yang dianggap sebagai Pencipta, yaitu Orang yang namanya: disebut dalam Ciptaan; dinyatakan sebagai Pencipta pada suatu Ciptaan; disebutkan dalam surat pencatatan Ciptaan; dan/atau tercantum dalam daftar umum Ciptaan sebagai Pencipta.

Artinya bisa saja, tulisan anda, tanpa sepengetahuan anda didaftarkan oleh orang lain sebagai Ciptaannya. Ini jelas pencurian ya. Tapi anda tetap bisa membuktikan bahwa andalah yang pertama kali menulisnya bila anda umumkan. Dalam hal ini pengumuman bisa dengan cara menerbitkannya sebagai buku atau naskah ilmiah di jurnal atau seminar, atau sesederhana dengan mengunggahnya ke repositori terbuka agar dapat ditemukan oleh mesin pencari dengan mudah.

- Kecuali terbukti sebaliknya, Orang yang melakukan ceramah yang tidak menggunakan bahan tertulis dan tidak ada pemberitahuan siapa Pencipta ceramah tersebut dianggap sebagai Pencipta.

Ini membingungkan bagi dosen/peneliti, karena kita dididik untuk selalu berusaha mencari siapa penulis suatu karya sampai ke sumber aslinya. Dengan perangkat mesin pencari dan berbagai basis data ilmiah, mestinya pencarian tidak sulit asal dokumen yang dicari tayang daring. **Jadi daring adalah KOENTJI**. 

- Dalam hal Ciptaan terdiri atas beberapa bagian tersendiri yang diciptakan oleh 2 (dua) Orang atau lebih, yang dianggap sebagai Pencipta yaitu Orang yang memimpin dan mengawasi penyelesaian
seluruh Ciptaan.
- Dalam hal Orang yang memimpin dan mengawasi penyelesaian seluruh Ciptaan sebagaimana dimaksud pada ayat (1) tidak ada, yang dianggap sebagai Pencipta yaitu Orang yang menghimpun Ciptaan dengan tidak mengurangi Hak Cipta masing-masing atas bagian Ciptaannya.

Ini akan membingungkan untuk dosen/peneliti yang kebiasaannya adalah bahwa yang menulis adalah penciptanya, sehingga orang yang tidak ikut menulis tidak dapat dianggap sebagai pencipta suatu karya tulis. Solusi dari kebingungan ini adalah dengan mendeskripsikan peran masing-masing seluruh tim penulis, termasuk yang bekerja secara langsung dalam proses pengambilan data atau analisis, tapi tidak menyumbangkan satu katapun ke dalam karya tulis. Kondisi seperti ini sangat mungkin terjadi pada tim riset besar, yang terdiri dari puluhan bahkan ratusan orang. Setelah semua didata, kemudian disepakati siapa yang ditunjuk sebagai perwakilan Tim Pencipta. Kita dapat mengadopsi taksonomi kontributor riset menurut [Casrai (Credit)](http://casrai.org). Perjanjian tertulis sangat diperlukan terutama bila kegiatan ini menghasilkan ciptaan yang bernilai ekonomi tinggi. 

- Dalam hal Ciptaan dirancang oleh seseorang dan diwujudkan serta dikerjakan oleh Orang lain di bawah pimpinan dan pengawasan Orang yang merancang, yang dianggap Pencipta yaitu Orang yang merancang Ciptaan.

Peraturan di atas sangat masuk akal, walaupun sebenarnya intinya adalah kesepakatan yang perlu didukung perjanjian tertulis tentang siapa yang ditunjuk sebagai perwakilan Tim Pencipta, terutama untuk ciptaan yang bernilai ekonomi tinggi.

- Kecuali diperjanjikan lain Pemegang Hak Cipta atas Ciptaan yang dibuat oleh Pencipta dalam hubungan dinas, yang dianggap sebagai Pencipta yaitu instansi pemerintah.

Baik. Ini banyak ditanyakan. Kata-kata "dinas" di sini dihubungkan dengan "instansi pemerintah". Jadi sepertinya dapat dihubungkan dengan seorang pegawai negeri sipil yang menciptakan atau menulis suatu karya atas penugasan kantornya yang merupakan kantor pemerintah. Dalam kasus dosen/peneliti di perguruan tinggi negeri (PTN). Menurut saya, pasal ini dapat diartikan: 

1. Kalau merujuk ke pasal 31, maka pencipta adalah orang yang namanya disebutkan dalam ciptaan (misal makalah ilmiah) yang bertindak atas nama instansi atau lembaga tempat ia bekerja. Jadi bila Si B adalah PNS penulis makalah dari PTN A, maka Si B adalah pencipta dari suatu karya tulis sebagai pegawai PTN A. Artinya dalam berbagai kegiatan, Si B harus mempertimbangkan posisi dan berbagai kepentingan PTN A, termasuk di dalamnya adalah ketika ia mengalihkan HCnya ke pihak lain (misal: penerbit jurnal). Artinya pula bahwa Si B harus memperhatikan konsekuensi berbagai hak ekonomi PTN A (bahkan negara) yang hilang, ketika ia mengalihkan HC atas karyanya; 
2. Bahwa penciptanya memang adalah instansi pemerintah mewakili negara. Artinya Si B tidak berhak atas karya yang ia buat sendiri. Dalam interpretasi ini, maka mestinya instansi pemerintah/instansi negara akan mengambil alih HC dan mereka tidak akan mengalihkannya ke pihak lain (misal: penerbit jurnal).

Pasal di atas, dilihat dengan interpretasi no (1) atau no (2) memang membingungkan di zaman sekarang, ketika semua penulis makalah (dosen/peneliti) bertindak atas nama pribadi. Ketika saya melihat kembali SK PNS, walaupun tidak tertulis di bagian inti, tetapi kalau ditengok di regulasi-regulasi terkait yang tercantum di bagian "Mengingat" dan "Memperhatikan", maka semangatnya adalah saya diangkat sebagai PNS sebagai perwakilan pemerintah/negara, sehingga saya bertindak atas nama pemerintah/negara. Kalau melihat ini, maka tanggungjawab kita sebagai PNS dosen/peneliti sangat berat, karena harus mempertimbangkan posisi pemerintah/negara dalam setiap perilaku kita, termasuk di dalamnya adalah ketika akan menandatangani **Copyright Transfer Agreement**.    

- Dalam hal Ciptaan sebagaimana dimaksud pada ayat (1) digunakan secara komersial, Pencipta dan/atau Pemegang Hak Terkait mendapatkan imbalan dalam bentuk Royalti.

Ini menarik, karena berkaitan dengan pasal sebelumnya (masalah Instansi Pemerintah sebagai Pencipta). Ketika sebuah HC yang dihasilkan oleh seorang dosen/peneliti PNS yang bekerja di sebuah instansi pemerintah (termasuk PTN), maka bila Ciptaan tersebut digunakan secara komersial, maka Pencipta (instansi pemerintah) mendapatkan imbalan dalam bentuk Royalti. **Nah apakah ketika kita menerbitkan makalah secara non OA dan OA (dengan APC), artinya Ciptaan Pemerintah itu dikomersialkan. Jawabnya adalah iya**.   

- Ketentuan lebih lanjut mengenai pemberian Royalti untuk penggunaan secara komersial sebagaimana dimaksud pada ayat (2) diatur dengan Peraturan Pemerintah.
- Kecuali diperjanjikan lain, Pencipta dan Pemegang Hak Cipta atas Ciptaan yang dibuat dalam hubungan kerja atau berdasarkan pesanan yaitu pihak yang membuat Ciptaan.

Jadi jelas dalam kondisi baku (tanpa perjanjian yang menyebutkan secara eksplisit tentang siapa yang memegang HC), maka ketika saya misalnya diminta menggambar untuk sebuah rapat atas permintaan panitia, maka saya adalah pemegang HC atas gambar saya. Kecuali ada perjanjian kerja tertulis yang menyatakan bahwa seluruh gambar yang saya buat atas penugasan panitia, adalah milik panitia. Hal ini juga berlaku bila ada anda diminta membuat logo suatu perusahaan.

- Kecuali terbukti sebaliknya, dalam hal badan hukum melakukan Pengumuman, Pendistribusian, atau Komunikasi atas Ciptaan yang berasal dari badan hukum tersebut, dengan tanpa menyebut seseorang sebagai Pencipta, yang dianggap sebagai Pencipta yaitu badan hukum.

Dalam kasus hubungan kerja saya dengan panitia rapat (yang bisa saja sebuah badan hukum), pasal ini akan sangat bergantung kepada niat baik badan hukum tersebut. Mereka tetap bisa mengumumkan gambar saya yang menceritakan kesimpulan sebuah rapat dengan tetap memasang nama saya sebagai penggambarnya.

### Tentang Ciptaan

**Ciptaan yang dilindungi HCnya**

Di sini dijelaskan tentang apa saja yang disebut sebagai Ciptaan yang dilindungi dan apa saja Ciptaan yang tidak dilindungi UU HC. Adanya di pasal 40.

- Ciptaan yang dilindungi meliputi:
    - Ciptaan dalam bidang ilmu pengetahuan, seni, dan sastra, terdiri atas: buku, pamflet, perwajahan karya tulis yang diterbitkan, dan semua hasil karya tulis lainnya;
    ceramah, kuliah, pidato, dan Ciptaan sejenis lainnya;
    - alat peraga yang dibuat untuk kepentingan pendidikan dan ilmu pengetahuan;
    - lagu dan/atau musik dengan atau tanpa teks;
    - drama, drama musikal, tari, koreografi, pewayangan, dan pantomim;
    - karya seni rupa dalam segala bentuk seperti lukisan, gambar, ukiran, kaligrafi, seni pahat, patung, atau kolase;
    - karya seni terapan; karya arsitektur; peta; karya seni batik atau seni motif lain; karya fotografi; Potret; karya sinematografi;
    - **(khusus)** terjemahan, tafsir, saduran, bunga rampai, basis data, adaptasi, aransemen, modifikasi dan karya lain dari hasil transformasi; terjemahan, adaptasi, aransemen, transformasi, atau modifikasi ekspresi budaya tradisional;
    - kompilasi Ciptaan atau data, baik dalam format yang dapat dibaca dengan Program Komputer maupun media lainnya; kompilasi ekspresi budaya tradisional selama kompilasi tersebut
    merupakan karya yang asli;
    - permainan video; dan Program Komputer.
- Ciptaan yang ditandai "(khusus)" merupakan karya turunan dari sebuah Ciptaan asli/orisinal. Ciptaan turunan ini dilindungi sebagai Ciptaan tersendiri dengan tidak mengurangi Hak Cipta atas
Ciptaan asli.
- Pelindungan berbagai jenis Ciptaan yang disebutkan di atas adalah termasuk pelindungan terhadap Ciptaan yang tidak atau belum dilakukan Pengumuman (**bukan Pendaftaran**) tetapi sudah diwujudkan dalam bentuk nyata yang memungkinkan Penggandaan Ciptaan tersebut.

A**pa yang bisa anda disimpulkan?**

- Bahwa terlihat berbagai jenis karya dosen dan peneliti dapat dikategorikan sebagai Ciptaan. Jadi jangan ragu lagi untuk berkarya. Sebagai dosen dan peneliti, karya anda tidak terbatas hanya kepada karya tulis, yang sering diartikan secara konvensional sebagai makalah yang terbit di jurnal atau prosiding seminar. Tidak lebih luas dari itu. Bahan kuliah daringpun (baik yang tayang dalam bentuk tertulis maupun audio visual) adalah hasil olah pikir yang dilindungi oleh UU HC.
- Saat anda melakukan translasi (alih bahasa) terhadap suatu karya orang asing, itupun dilindungi oleh UU HC, selama proses translasinya tidak menyontek pihak lain.
- Apa yang lebih didulukan? Mengumumkan atau membuat suatu karya terwujud (ada wujudnya)? Anda harus mewujudkannya ke dalam bentuk yang konkrit/nyata, sebelum mengumumkannya. Syarat utama agar suatu Ciptaan dapat dilindungi adalah bila ia telah memiliki wujud.

**Tentang Karya yang Tidak Dilindungi Hak Cipta**

Bagian yang dinyatakan dalam Pasal 41 ini terus terang membingungkan, kecuali no 1. Bukankah jenis karya no 2 dan 3, ketika diwujudkan menjadi obyek nyata maka bisa masuk ke dalam kategori karya yang dilindungi HCnya.

Hasil karya yang tidak dilindungi Hak Cipta meliputi:

1. hasil karya yang belum diwujudkan dalam bentuk nyata;
2. setiap ide, prosedur, sistem, metode, konsep, prinsip, temuan atau
data walaupun telah diungkapkan, dinyatakan, digambarkan,
dijelaskan, atau digabungkan dalam sebuah Ciptaan; dan
3. alat, benda, atau produk yang diciptakan hanya untuk menyelesaikan
masalah teknis atau yang bentuknya hanya ditujukan untuk
kebutuhan fungsional.

**Sepertinya kita tidak dapat menyimpulkan apa-apa dari sini, karena masih membingungkan.**

**Tentang Karya yang ada Hak Ciptanya**

Di pasal 42 dijelaskan berbagai karya yang tidak ada HCnya. Ternyata ada, yaitu beberapa karya sbb:

**Tidak ada Hak Cipta atas hasil karya berupa**:

1. hasil rapat terbuka lembaga negara;
2. peraturan perundang-undangan;
3. pidato kenegaraan atau pidato pejabat pemerintah;
4. putusan pengadilan atau penetapan hakim; dan
5. kitab suci atau simbol keagamaan.

**Walaupun daftar karya yang tidak ber-HC ini bisa dipahami logikanya, tetapi apakah dengan dinyatakan sebagai "tidak ber-HC"  dapat secara otomatis kita kelompokkan kepada barang domain publik (atau *public domain*)?**

### Tentang Hak Ekonomi

Masih bahwa HC terdiri dari Hak Moral dan Hak Ekonomi. Dalam UU 28/2014 yang disebutkan pertama adalah Hak Moral. Tapi sekarang kita lihat dulu Hak Ekonomi, baru setelahnya mundur ke Hak Moral. Meluncur ke Pasal 8 dan 9 dulu yang membahas Hak Ekonomi Pencipta atau Pemegang Hak Cipta.

- Hak ekonomi merupakan hak eksklusif Pencipta atau Pemegang Hak Cipta
untuk mendapatkan manfaat ekonomi atas Ciptaan.
- Pencipta atau Pemegang Hak Cipta memiliki hak ekonomi untuk melakukan: penerbitan Ciptaan; Penggandaan Ciptaan dalam segala bentuknya; penerjemahan Ciptaan; pengadaptasian, pengaransemenan, atau pentransformasian Ciptaan; Pendistribusian Ciptaan atau salinannya; pertunjukan Ciptaan; Pengumuman Ciptaan; Komunikasi Ciptaan; dan penyewaan Ciptaan.
- Setiap Orang yang melaksanakan hak ekonomi sebagaimana dimaksud pada ayat (1) wajib mendapatkan izin Pencipta atau Pemegang Hak Cipta.
- Setiap Orang yang tanpa izin Pencipta atau Pemegang Hak Cipta dilarang melakukan Penggandaan dan/atau Penggunaan Secara Komersial Ciptaan.

**Menurut anda, apa yang dapat kita simpulkan?**

- Pemegang HC Hak Ekonomi dapat melakukan apa saja kepada ciptaannya. Siapapun yang akan melakukan hal yang sama, diwajibkan mendapatkan izin dari pemegang HC, dalam hal ini adalah pencipta. Di sinilah makna kata "eksklusif".
- Sekarang: apa yang terjadi bila HC dialihkan? Nanti kita lihat sama-sama.

### Tentang Hak Moral

Kita mundur ke belakang ke Bab II Hak Cipta UU 28/2014 tentang Hak Moral.

- Hak Cipta merupakan hak eksklusif yang terdiri atas hak moral dan hak ekonomi. Tentang Hak Moral: Hak moral merupakan hak yang melekat secara abadi pada diri Pencipta untuk:
    - tetap mencantumkan atau tidak mencantumkan namanya pada
    salinan sehubungan dengan pemakaian Ciptaannya untuk
    umum;
    - menggunakan nama aliasnya atau samarannya;
    - mengubah Ciptaannya sesuai dengan kepatutan dalam masyarakat;
    - mengubah judul dan anak judul Ciptaan; dan
    - mempertahankan haknya dalam hal terjadi distorsi Ciptaan,
    mutilasi Ciptaan, modifikasi Ciptaan, atau hal yang bersifat
    merugikan kehormatan diri atau reputasinya.
    - Hak moral tidak dapat dialihkan selama Pencipta masih hidup, tetapi pelaksanaan hak tersebut dapat dialihkan dengan wasiat atau sebab lain sesuai dengan ketentuan peraturan perundang-undangan setelah Pencipta meninggal dunia.
    - Dalam hal terjadi pengalihan pelaksanaan hak moral sebagaimana dimaksud pada ayat (2), penerima dapat melepaskan atau menolak pelaksanaan haknya dengan syarat pelepasan atau penolakan pelaksanaan hak tersebut dinyatakan secara tertulis.
- Untuk melindungi hak moral, Pencipta dapat memiliki: a. informasi manajemen Hak Cipta; dan/atau b. informasi elektronik Hak Cipta.
- Informasi manajemen Hak Cipta, meliputi informasi tentang: a. metode atau sistem yang dapat mengidentifikasi originalitas substansi Ciptaan dan Penciptanya; dan b. kode informasi dan kode akses.
- Informasi elektronik Hak Cipta meliputi informasi tentang: a. suatu Ciptaan, yang muncul dan melekat secara elektronik dalam hubungan dengan kegiatan Pengumuman Ciptaan; b. nama pencipta, aliasnya atau nama samarannya; c. Pencipta sebagai Pemegang Hak Cipta; d. masa dan kondisi penggunaan Ciptaan; e. nomor; dan f. kode informasi.
- Informasi manajemen Hak Cipta sebagaimana dimaksud pada ayat (1) dan informasi elektronik Hak Cipta sebagaimana dimaksud pada ayat (2) yang dimiliki Pencipta dilarang dihilangkan, diubah, atau dirusak.

**Sampai ini apa yang dapat kita simpulkan tentang Hak Moral?**

- Bahwa Hak Moral tidak dapat dilepaskan dari seorang pencipta, karena itulah hak yang paling mendasar. Sampai kapanpun, pemegang Hak Moral atas suatu ciptaan akan selalu diakui sebagai pencipta untuk seluruh karyanya. Tapi dapat dilihat dari rincian pasal di atas, Hak Moral ini hanya melingkupi hal-hal yang administratif.
- Bandingkan antara rincian Hak Moral dengan Hak Ekonomi yang dibahas sebelumnya. Terlihat bedanya bukan.

### Tentang pembatasan HC

Di pasal 43 dijelaskan beberapa hal yang tidak tergolong pelanggaran HC. 

Perbuatan yang tidak dianggap sebagai pelanggaran Hak Cipta meliputi:

- Pengumuman, Pendistribusian, Komunikasi, dan/atau Penggandaan lambang negara dan lagu kebangsaan menurut sifatnya yang asli;
- Pengumuman, Pendistribusian, Komunikasi, dan/atau Penggandaan segala sesuatu yang dilaksanakan oleh atau atas nama pemerintah, kecuali dinyatakan dilindungi oleh peraturan perundang-undangan, pernyataan pada Ciptaan tersebut, atau ketika terhadap Ciptaan
tersebut dilakukan Pengumuman, Pendistribusian, Komunikasi, dan/atau Penggandaan;

Ayat di atas menarik. Saya membayangkan ketika di masa pandemi Covid ini banyak terbit makalah non OA (all rights reserved), kemudian atas permintaan pemerintah, para penerbit diminta membuka akses ke makalah-makalah non OA terkait Covid. Kalau melihat ayat di atas, maka mestinya pemerintah dapat melakukannya. 

- pengambilan berita aktual, baik seluruhnya maupun sebagian dari kantor berita, Lembaga Penyiaran, dan surat kabar atau sumber sejenis lainnya dengan ketentuan sumbernya harus disebutkan secara lengkap; atau

Ayat di atas cukup masuk akal, seperti kita melakukan penyitiran pada umumnya. 

- pembuatan dan penyebarluasan konten Hak Cipta melalui media teknologi informasi dan komunikasi yang bersifat tidak komersial dan/atau menguntungkan Pencipta atau pihak terkait, atau Pencipta tersebut menyatakan tidak keberatan atas pembuatan dan penyebarluasan tersebut.

Nah ayat di atas bisa kita aplikasikan untuk materi kuliah (teks, audio atau visual) yang dibagikan di media yang terbuka dan bisa diakses umum. Kalau menilik ayat di atas, maka video ajar berisi material ber HC milik orang lain, sebagai pendukung, dapat dibagikan di media komunikasi Youtube, selama tidak dikomersialkan. Dalam kasus Youtube adalah dimonetisasi.  

- Penggandaan, Pengumuman, dan/atau Pendistribusian Potret Presiden, Wakil Presiden, mantan Presiden, mantan Wakil Presiden, Pahlawan Nasional, pimpinan lembaga negara, pimpinan kementerian/lembaga pemerintah non kementerian, dan/atau kepala daerah dengan memperhatikan martabat dan kewajaran sesuai dengan ketentuan peraturan perundang-undangan.

### Tentang pengalihan Hak Ekonomi

Pengalihan Hak Ekonomi dijelaskan pada pasal 16-19, 29 dan 30.

- Dijelaskan bahwa "Hak ekonomi atas suatu Ciptaan tetap berada di tangan Pencipta atau Pemegang Hak Cipta selama Pencipta atau Pemegang Hak Cipta **tidak mengalihkan** seluruh hak ekonomi dari Pencipta atau Pemegang Hak Cipta tersebut kepada penerima pengalihan hak atas Ciptaan. Pengalihan Hak Ekonomi hanya dapat dilakukan satu kali, dengan perjanjian tertulis.
- "Ciptaan buku, dan/atau semua hasil karya tulis lainnya, lagu dan/atau musik dengan atau tanpa teks yang dialihkan dalam perjanjian jual putus dan/atau pengalihan tanpa batas waktu, Hak Ciptanya beralih kembali kepada Pencipta pada saat perjanjian tersebut mencapai jangka waktu 25 (dua puluh lima) tahun." Nah ini sangat terkait dengan dunia publikasi akademik. Jadi ketika kita akan menerbitkan secara non OA dan untuk itu harus menandatangani Perjanjian Pengalihan HC atau *Copyright Transfer Agreement*, maka HCnya akan kembali kepada penulis sebagai pemegang HC awal setelah 25 tahun. Sekarang kita lihat, ada berapa banyak makalah dari penulis Indonesia yang berusia lebih dari 25 tahun sejak diterbikan yang HCnya masih di tangan penerbit. Mestinya karya-karya itu HCnya kembali ke penulis aslinya, atau diperpanjang pengalihannya dengan perjanjian tertulis yang baru. Keputusan untuk menjadikannya domain publik mestinya ada pada penulis.

### Tentang lisensi dan royalti

Beberapa hal tentang lisensi dan royalti diatur dalam Bab XI atau pasal 80 sampai 86. Saya akan sampaikan beberapa hal yang penting terkait publikasi ilmiah saja.

- Bahwa lisensi adalah izin yang diberikan oleh pencipta. Itu adalah hak pencipta. Bahwa terkait dengan royalti (kewajiban membayar dari pengguna ke pencipta), ada lisensi yang diberikan tanpa menarik royalti, ada pula yang menarik royalti. Itupun hak pencipta yang dituangkan dalam bentuk perjanjian. Perjanjian lisensi (termasuk royaltinya) berlaku tidak melebihi masa berlaku HC.
- Perjanjian lisensi tidak boleh berisi ketentuan yang merugikan perekonomian Indonesia atau menjadi sarana untuk mengurangi, menghilangkan, atau mengambil alih seluruh hak pencipta. Perjanjian ini harus didaftarkan ke kementerian terkait.
- Nah ini ada yang menarik di pasal 84 dan 85 tentang Lisensi Wajib. Pasal 84: "Lisensi wajib merupakan Lisensi untuk melaksanakan penerjemahan dan/atau Penggandaan Ciptaan dalam bidang ilmu pengetahuan dan sastra yang diberikan berdasarkan keputusan Menteri atas dasar permohonan untuk kepentingan pendidikan dan/atau ilmu pengetahuan serta kegiatan penelitian dan pengembangan". Pasal 85: "Setiap Orang dapat mengajukan permohonan lisensi wajib terhadap Ciptaan dalam bidang ilmu pengetahuan dan sastra sebagaimana dimaksud dalam Pasal 84 untuk kepentingan pendidikan, ilmu pengetahuan, serta kegiatan penelitian dan pengembangan kepada Menteri". Dalam dua pasal di atas, kita atas nama hukum dan kepentingan pengembangan ilmu pengetahuan (pendidikan dan penelitian) dapat mengajukan lisensi penerjemahan dan penggandaan ciptaan, misal: menerjemahkan buku, perangkat lunak.
- **Pertanyaan saya apakah ini berarti kita dapat mengajukan setidaknya satu versi gratis untuk pendidikan (free educational version) untuk perangkat lunak komputer yang sangat penting?**
- Pada pasal 86 dinyatakan periode masa edar minimum ciptaan sebelum kita dapat mengajukan permohonan lisensi untuk penerjemahan atau penggandaan, yaitu tiga tahun. Anehnya yang disebutkan adalah hanya untuk ciptaan berupa buku. Padahal ciptaan bisa juga perangkat lunak komputer. **Apakah saya melewatkan sesuatu?**

## **BAGIAN 1 HKI DARI SUDUT PANDANG PENGGUNA**

Bagian ini telah tayang lebih awal pada tautan ini [dasaptaerwin-HKI](https://www.notion.so/Hak-Kekayaan-Intelektual-HKI-untuk-dosen-dan-peneliti-7a027d8cd86545efab2454169c64c775), yang terdiri dari beberapa materi sbb: 

- Dasar-dasar HKI. Apa saja yang boleh dan tidak boleh? Di sini saya menjelaskan beberapa hal yang mendasar tentang HKI, serta cuplikan pemikiran tentang apa yang seharusnya dilakukan oleh dosen/peneliti sebagai pengguna dan sebagai pencipta.

[https://youtu.be/bFi3vWo1A_g](https://youtu.be/bFi3vWo1A_g)

- Mencari materi yang legal untuk digunakan ulang secara langsung dan legal? Di sini saya menyampaikan cara mencari material yang dapat digunakan secara langsung (tanpa izin) tapi legal.
- Saya menyarankan untuk mencari material dari dokumen (buku/makalah OA). Makalah OA yang saya maksud adalah yang OA sejak lahir, bukan yang seolah OA. Dokumen yang seolah OA misal:
    - Dokumen yang sebenarnya non OA tapi dapat diunduh pembaca secara penuh dan langsung seperti OA, karena penulis mengunggah dokumen tersebut ke  ResearchGate, Academia.edu, atau media sosial lainnya.
    - Dokumen-dokumen yang ada di RG dan Academia seringnya berupa dokumen hasil kerja penulis yang terbit secara non OA dan HKInya telah dialihkan ke pihak lain (penerbit). Dokumen-dokumen yang HKInya telah dialihkan ke pihak lain (penerbit), walaupun dapat diunduh secara lengkap oleh pembaca, tetap tidak dapat digunakan tanpa izin pemegang HKI (penerbit). Beberapa pengecualian "Fair Use" dapat digunakan, namun ini sifatnya adalah toleransi dari penerbit (umumnya penerbit komersial), bukan pengguguran kewajiban untuk meminta izin.

[https://youtu.be/YUdTKkVqwT4](https://youtu.be/YUdTKkVqwT4)

- Bagaimana cara minta izin ke pemegang HKI? Di sini saya mendemokan permohonan izin satu gambar yang ada dalam buku non OA terbitan Springer.

[https://youtu.be/J_L4E-yrNnI](https://youtu.be/J_L4E-yrNnI)

## **BAGIAN 2 HKI DARI SUDUT PANDANG PENCIPTA (PENULIS)**

Bagian ini juga telah tayang lebih awal pada tautan ini [dasaptaerwin-HKI](https://www.notion.so/Hak-Kekayaan-Intelektual-HKI-untuk-dosen-dan-peneliti-7a027d8cd86545efab2454169c64c775), yang terdiri dari beberapa materi sbb: 

- Perbedaan kebijakan HKI penerbit komersial (Elsevier) dan penerbit asosiasi profesi (RSC dan AGU). Di sini saya menjelaskan bahwa penerbit komersial ada kecenderungan untuk memperluas lingkup pengalihan HC, sementara penerbit asosiasi profesi (yang diasumsikan beroperasi secara nirlaba) cenderung mengalihkan HC dari penulis dalam lingkup yang cara wajar.

[https://youtu.be/NYjiWW5_m0I](https://youtu.be/NYjiWW5_m0I)

- Tentang perjanjian pengalihan Hak Cipta (*copyrights transfer agreement*). Di sini saya menjelaskan beberapa hal mendasar tentang proses pengalihan HC yang biasa dialami para dosen/peneliti ketika akan menerbitkan makalah atau buku secara non OA.

[https://youtu.be/s_gqUk9k3X4](https://youtu.be/s_gqUk9k3X4)

- Dalam video tersebut di atas saya juga menyarankan bagi para dosen/peneliti sebagai pencipta untuk:
    - menyadari bahwa proses publikasi yang secara dilaksanakan, seringkali mengabaikan masalah HKI, terutama hak-hak yang dimiliki penulis yang kemudian dialihkan serta hak-hak yang masih tersisa dan perlu dimaksimumkan untuk kepentingan pengembangan ilmu.
    - CTA biasanya hanya akan dilakukan bila kita akan menerbitkan makalah atau buku secara non OA. Konsekuensinya kita akan kehilangan sebagian besar Hak Ekonomi, walaupun Hak Moral masih dipegang.
    - Pilihan kita saat disodori CTA, dalam kondisi berbagai aturan kementerian yang tidak pernah memperhatikan pengalihan HC adalah tetap menandatanganinya (karena memang itu yg diminta peraturan dikti agar sesuai dengan aturan publikasi) dengan meminimkan "kerugian".
    - Caranya meminimkan "kerugian":
        - Unggah draft makalah atau draft buku dan unggah pula versi yg lolos peer review juga ke repositori terbuka. kenapa? agar pembaca bisa mendapatkan versi OAnya secara legal. Kita juga kan repot kalau harus minta versi OA ke penulis. tujuan lainnya adalah untuk memaksimumkan hak milik bapak, karena versi ini seluruh HCnya masih di tangan bapak,
        - Tempelkan lisensi CC-BY ke dokumen itu, agar pembaca tidak perlu repot minta izin kepada kita. kenapa? karena kita sendiri sekarang kan merasa repot harus minta izin.

## BAGIAN 3 LIMA BELAS (mungkin bisa bertambah) HAL YANG JARANG ATAU TIDAK PERNAH DISAMPAIKAN NARSUM HKI

1. Para narsum **hanya fokus ke Hak kekayaan industri**. Padahal HKI atau IPR terdiri dari Hak Cipta dan Hak Kekayaan Industri (misal paten merek rahasia dagang varietas tanaman dll yg sejenisnya),
2. Kalaupun Hak Cipta (HC) dibahas tapi tetap saja **dibahas seolah menjadi bagian dari Hak Kekayaan Industri**. Mayoritas narsum dari perguruan tinggi tapi malah hampir tidak pernah membahas HC dalam kaitan dengan makalah dan berbagai bentuk karya tulis lainnya.
3. Bukti bahwa narsum sering membahas HC seolah masuk ke Kekayaan Industri adalah dengan **seringnya menyebut bahwa HC harus didaftarkan**. Padahal HC tidak perlu, tidak harus, dan tidak wajib didaftarkan. Yang perlu didaftarkan adalah Hak Kekayaan Industri, yaitu: paten, merek, rahasia dagang dll.
4. Kenapa harus didaftarkan? Karena **hak kekayaan industri sarat dan hak ekonomi dan motivasi ekonomi**.
5. Hak Ekonomi merupakan salah satu hak yang termasuk ke dalam HC. Yang lainnya adalah **Hak Moral**, yaitu hak untuk diakui sebagai pencipta suatu karya.
6. Narsum seringkali menghubungkan antara publikasi (publikasi ilmiah) dengan masa pendaftaran paten. Kalau material yang akan dipatenkan sudah dipublikasikan (misal sebagai makalah ilmiah), maka tidak dapat lagi didaftarkan. Padahal publikasi dalam bentuk apapun adalah hal wajib dalam kegiatan riset khususnya yang didanai negara. Terlepas dari banyaknya syarat publikasi yang diakui, tetapi mempublikasikan riset di berbagai kanal adalah kewajiban. Menghambat publikasi dengan alasan apapun termasuk bahwa di dalamnya terkait material yang akan dipatenkan, akan menyalahi prinsip hasil riset sebagai barang publik, karena dibiayai negara.
7. Narsum kebanyakan menyebut pendaftaran HKI (paten) sbg salah satu bukti reputasi. Padahal HKI paten lahir dari upaya merahasiakan atau menahan informasi secara lengkap dari hasil riset yang didanai negara. Tentunya pendapat saya ini tidak berlaku untuk HKI paten yang didanai pabrik obat.
8. Narsum sebagai kaum pendidik justru mengarahkan intonasi pembicaraan ke arah bahwa luaran penelitian atau PKM bisa diarahkan untuk menghasilkan paten. Padahal argumentasi no 7 mestinya itu yang diangkat. Terlebih lagi, kegiatan PKM (pengabdian kepada masyarakat) kok diminta menghasilkan paten.
9. Narsum banyak yg bilang kalau riset akan bagus kalau menghasilkan HKI paten padahal mestinya argumentasi no 7 yg seharusnya disampaikan.
10. Narsum jarang bahkan tidak pernah menyampaikan bahwa HC dapat dilonggarkan dengan memberikan lisensi (izin). Kalaupun penjelasan tentang lisensi disampaikan, tapi pasti dihubungkan dengan Hak Ekonomi berupa penarikan royalti. Semua kegiatan tampaknya ditargetkan untuk mendapatkan keuntungan finansial.
11. Terkait dengan no 10, narsum seringnya hanya menyampaikan bahwa HC tidak dapat digunakan ulang tanpa izin pemegang HC, dan pencipta (peneliti) selalu didorong untuk mendaftarkan HC, padahal setiap hari kita selalu senang ada banyak orang baik yang membagikan kode program di [Github](http://github.com) atau [Gitlab](http://gitlab.com), membagikan PPT seminaring, membagikan foto dan gambar yang bagus di [Pixabay](http://pixabay.com).
12. Terkait dengan no 10 dan 11, narsum jarang bahkan tidak pernah menyampaikan ada lisensi terbuka seperti [Creative Commons](http://creativecommons.org), MIT license, GNU Public License, dll yang tertempel di setiap gambar/foto dari Pixabay, kode program di Github, Sistem Operasi Linux, perangkat lunak gratis VIM atau Emacs.
13. HKI paten sering digambarkan sebagai proses yang mudah. Padahal dibalik proses pendaftaran "yang mudah" ada proses pemeliharaan paten (ketika paten disetujui) yang tidak mudah dan tidak murah. Kalaupun murah, tetap saja tidak dalam kapasitas pribadi peneliti untuk membiayai. Akhirnya mungkin akan berakhir dengan mengajukan dana lagi ke negara. Jadi negara membiayai sejak dari penelitian, publikasi, pendaftaran paten, pemeliharaan paten, dan tentunya honor dan insentif untuk para peneliti. Sementara kita sangat sensitif ketika dikenakan pajak ganda oleh negara.
14. Para narsum seringnya bilang bahwa HC (karya tulis) penting, tapi tidak ada yang menyampaikan bahwa HC ini bisa jadi mematikan ketika setiap pengguna yang ingin menggunakan ulang, harus minta izin. HC ini juga bisa menjadi obyek monetisasi pihak lain tanpa memberikan bagi hasil sedikitpun ke pencipta, bahkan sampai pencipta meninggal dunia. Hal inipun tidak pernah disampaikan.
15. Walaupun banyak narsum berasal dari kalangan akademik dan sudah mendapatkan pelatihan HKI sampai ke LN, tapi tidak ada yang menyampaikan bagaimana HKI Hak Cipta tetap bisa dipertahankan tanpa menghalangi pihak lain menggunakan ulang bahkan mengembangkan ciptaan.

## Tanya Jawab Umum (TJU) tentang HKI

TJU/FAQ dapat dilihat [di sini](http://dasaptaerwin.net/wp/wp-content/uploads/2020/07/TJU-FAQ-HKI-1.xlsx).