# HKI untuk dosen dan peneliti

Repositori ini menyimpan dokumen ulasan saya tentang Hak Kekayaan Intelektual (HKI) untuk dosen dan peneliti. Materi HKI yang saya sampaikan ini adalah sisi yang jarang disampaikan oleh narsum/konsultan HKI. Pembaca dapat langsung mengunduh dokumen: HKI-dosen-dan-peneliti format `docx` atau `pdf`. Karena repositori ini bersifat bebas pakai (_public domain_), maka kedua dokumen tersebut juga dapat digunakan untuk kepentingan pribadi atau dipasang menjadi laman situs perpustakaan perguruan tinggi ibu dan bapak. Semoga bermanfaat. 

